FROM python:3.5-stretch

RUN set -e; \
    apt-get update -y; \
    apt-get upgrade -y; \
    apt-get install -y --no-install-recommends fop;

COPY web_crm web_crm/
COPY . web_crm/
COPY requirements.txt web_crm/requirements.txt
COPY requirements-prod.txt web_crm/requirements-prod.txt
COPY requirements-git.txt web_crm/requirements-git.txt



EXPOSE 8000
EXPOSE 5432

COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /web_crm

RUN pip install --upgrade pip

RUN pip install -r requirements-git.txt

RUN pip install -r requirements-prod.txt

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]



CMD ["gunicorn", "--log-file=/var/log/gunicorn.log", "web_crm.wsgi", "-b 0.0.0.0:8000"]
