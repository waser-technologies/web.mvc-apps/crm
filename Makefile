include ./.env

env:
	pip install --upgrade pip
	pip install -r requirements.txt
	pip install -r requirements-git.txt
	pip install -r requirements-debug.txt

static:
	. ./.env
	python ./manage.py collectstatic --no-input

docker_build:
	. ./.env
	#docker-machine start default
	eval "$(docker-machine env default)"
	docker build -t $(DOCKER_IMAGE) .

docker_push:
	. ./.env
	docker login -u $(DOCKER_USER) -p $(DOCKER_PASSWORD)
	#docker-machine start default
	eval "$(docker-machine env default)"
	docker push $(DOCKER_IMAGE)


init:
	. ./.env
	python ./manage.py migrate --no-input
	python ./manage.py collectstatic --no-input
	python ./manage.py createsuperuser

migrations:
	. ./.env
	python ./manage.py makemigrations --no-input

migrate:
	. ./.env
	python ./manage.py migrate --no-input

server:
	. ./.env
	python ./manage.py runserver

messages:
	. ./.env
	python ./manage.py makemessages -l fr --ignore=static/* --ignore=env/*

translate:
	. ./.env
	python ./manage.py compilemessages -l fr
