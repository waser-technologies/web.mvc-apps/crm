# CRM

This web application is based upon the work of [Koalix-CRM](https://github.com/KoalixSwitzerland/koalixcrm).

## Web.crm

This is a roleback in time to adapt KoalixSwitzerlands's CRM/ERP into Web.mvc.

## Framework
### Model-View-Controller
Web.crm uses the Model-View-Controller design pattern, which is commonly used for developing user interfaces and has become very popular for web devloppment.
### Django-CMS
This Content Managment System based on [Django](https://docs.djangoproject.com/) is a powerful tool to easily create content for your website.

More information [@Django-CMS](http://docs.django-cms.org/en/latest/).
### Bootstrap 4
In order to modify content from the CMS, Bootstrap 4 as been choosen to stadardize HTML components. It also makes it easier to create your own templates.

Get more information about components [@Bootstrap](https://getbootstrap.com/docs/4.4/getting-started/introduction/).
## Templates
Web.crm is the core technology to create a website. It doesn't come with templates.

Templates and their configurations come bundled inside a Web.crm Theme.

Since I'm not a designer, I use Bootstrap Templates which I don't own to create Web.crm Themes. For that reason, I cannot share code for those Themes.

You can nonetheless use our blank Theme to easely create your own.

### Web.crm Base Theme
Bootstrap your own theme easily Web.crm Base Theme.

You can create your theme from scratch or use a Bootstrap 4 template. They are widely avalible on Internet.

Checkout the repo to get started [@Web.crm-base-theme](https://gitlab.com/waser-technologies/Web.crm-themes/Web.crm-base-theme)

Use the dedicated interface in the Web.crm administration.

## Install locally

Clone this repo and prepare the environment.

```zsh
mkdir Web.crm
cd Web.crm
git clone git@gitlab.com:waser-technologies/web.mvc-apps/crm.git ./src
virtualenv --python-packages=/usr/bin/python3.5 ./env
source env/bin/activate
cd src
make env
```

Initialize the database.
You only need to run this the first time.

```zsh
make init
```

Run the server with this command.

```zsh
make server
```

### Installing using Docker (Recommanded for testing)

Install [Docker](https://docs.docker.com/get-docker/) and compose on local network.

```zsh
docker-compose up -d --build docker-compose.debug.yml
```
The first time you'll need to initialize the database manualy.

```zsh
docker-compose run --rm web_crm /web_crm/manage.py migrate
docker-compose run --rm web_crm /web_crm/manage.py collectstatic --no-input
docker-compose run --rm web_crm /web_crm/manage.py createsuperuser
```

Done. Checkout [localhost](http://localhost:80/admin).

## Hosting
Web.crm is powered by Docker to allow you to easily build it anywhere you want.

Since Web.crm doesn't come with templates you can't deploy Web.crm on it's own. Checkout the [Deploy Web.mvc](https://gitlab.com/waser-technologies/Web.crm-themes/Web.crm-base-theme#deploy-webmvc) section of [Web.crm-base-theme](https://gitlab.com/waser-technologies/Web.crm-themes/Web.crm-base-theme).

If you need to modify Web.crm source code, publish a [Docker](https://docs.docker.com/get-docker/) image of your version of Web.crm. 

You would need a [Docker Hub](https://hub.docker.com) free account and to change thoses variables accordingly inside the .env file.

```
DOCKER_USER
DOCKER_PASSWORD
DOCKER_REPO
```
Now you can push your image to your docker repository.

`make image`

Don't forget to update the image name inside your theme's `.Dockerfile`.
