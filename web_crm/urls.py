# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals
import os

from django.conf.urls.static import *
from django.contrib.staticfiles.urls import static
from django.conf.urls import url
from django.contrib import admin
from filebrowser.sites import FileBrowserSite
from django.core.files.storage import DefaultStorage

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve


site = FileBrowserSite(name="filebrowser", storage=DefaultStorage())
customsite = FileBrowserSite(name='custom_filebrowser', storage=DefaultStorage())
customsite.directory = "uploads/"

admin.autodiscover()

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap,
        {'sitemaps': {'cmspages': CMSSitemap}}),
]

urlpatterns += i18n_patterns(
    url(r'^admin/filebrowser/', customsite.urls),
    url(r'^admin/', admin.site.urls),  # NOQA
    url(r'^accounts/', include('allauth.urls')),
    #url(r'^dashboard/', include('dashboard.urls')),
)

if os.environ.get('POSTGRES_HOST', False):
    urlpatterns += i18n_patterns(
        url(r'^accounting/hordak/', include('hordak.urls', namespace='hordak')),
        url(r'^accounting/VAT/', include('vat.urls')),
        url(r'^accounting/', include('accountent.urls')),
    )

urlpatterns += i18n_patterns(
    url(r'^', include('cms.urls')),
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns

